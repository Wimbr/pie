class Pie
{
  // internal variables
  
  int myRadius ;
  float myRotation = 0;
  float myAngle ; 
color myColor  = color(0,0,0);   



   
  // constructor
   Pie(int _radius, float _angle, float _rotation)
   {
     // store properties
     myRotation = _rotation ; 
     myRadius = _radius ;
     myAngle = _angle ; 
     // let's output a message to tell console i am alive
    // println("Pie instantiated with radius of" + _radius + " end angle of: " + _angle + " ." );
    // and now the serious business let's draw something
   // this.update();   
   }
   
   void setColor(color _c){
   myColor = _c ; 
   }
   
   void update(){
   
     //print("drawing");
     // no curves yet just plain old lines
   noStroke();
   
 
   pushMatrix();
   
    rotate(myRotation);
   
   /*line(0,0,0,myRadius);
   line(0,myRadius,myRadius*sin(myAngle),myRadius*cos(myAngle));
   line(myRadius*sin(myAngle),myRadius*cos(myAngle),0,0);
  */
 //  stroke(255,0,0);
   
   float _smallR= 2*myAngle/PI *.55*myRadius ; 
   
  fill(myColor);
   
   beginShape();
   vertex(0, 0);
   bezierVertex(0, 0, 0, myRadius, 0, myRadius);
 //  bezierVertex(2*myAngle/PI *.55*myRadius, myRadius, myRadius, 2*myAngle/PI*.55*myRadius, myRadius*sin(myAngle),myRadius*cos(myAngle));  
   bezierVertex(_smallR, myRadius,myRadius*sin(myAngle) -_smallR*cos(myAngle) ,myRadius*cos(myAngle) +_smallR*sin(myAngle) , myRadius*sin(myAngle),myRadius*cos(myAngle));  
   endShape(CLOSE);
   
   /*
   stroke(0,255,0);
   line( 0, myRadius,_smallR, myRadius);
   line(myRadius*sin(myAngle),myRadius*cos(myAngle),myRadius*sin(myAngle) -_smallR*cos(myAngle) ,myRadius*cos(myAngle) +_smallR*sin(myAngle) );
  */
 
  popMatrix();
  
   }
   
  
}
