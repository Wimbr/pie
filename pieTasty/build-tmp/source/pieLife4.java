import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class pieLife4 extends PApplet {


ArrayList pies ; 
  
  int Colors[];


Pie pie ;

public void setup(){
 
int Colors[] = new int[3];
Colors[0] = color(10,12,230);
Colors[1] = color(10,120,23);
Colors[2] = color(100,12,23); 
noStroke();

  
  pies = new ArrayList();  // Create an empty ArrayList
  for (int i = 0 ; i < 200 ; i ++ ){
  Pie _pie = new Pie((int) random(200), random(PI/16), random(PI*2));
  _pie.setColor(Colors[PApplet.parseInt(random(Colors.length))]);
  pies.add(_pie);  // Start by adding one element
  
}
size(500,500,OPENGL);
frameRate(25);
//noLoop();
}
public void draw()
{
 // delay(500);
 //pies.clear();

background(127);
translate(250,250);
drawCross();
for(int i = 0 ; i < pies.size() ; i ++){
  Pie pie = (Pie) pies.get(i);
pie.myAngle = random(PI/2);
pie.myRotation = random(2*PI);
pie.myRadius = (int) random(200);
pie.update();
}
}

public void drawCross()
{
stroke(255);
line(-10,0,10,0);
line(0,10,0,-10);


}
class Pie
{
  // internal variables
  
  int myRadius ;
  float myRotation = 0;
  float myAngle ; 
int myColor  = color(0,0,0);   



   
  // constructor
   Pie(int _radius, float _angle, float _rotation)
   {
     // store properties
     myRotation = _rotation ; 
     myRadius = _radius ;
     myAngle = _angle ; 
     // let's output a message to tell console i am alive
    // println("Pie instantiated with radius of" + _radius + " end angle of: " + _angle + " ." );
    // and now the serious business let's draw something
   // this.update();   
   }
   
   public void setColor(int _c){
   myColor = _c ; 
   }
   
   public void update(){
   
     //print("drawing");
     // no curves yet just plain old lines
   noStroke();
   
 
   pushMatrix();
   
    rotate(myRotation);
   
   /*line(0,0,0,myRadius);
   line(0,myRadius,myRadius*sin(myAngle),myRadius*cos(myAngle));
   line(myRadius*sin(myAngle),myRadius*cos(myAngle),0,0);
  */
 //  stroke(255,0,0);
   
   float _smallR= 2*myAngle/PI *.55f*myRadius ; 
   
  fill(myColor);
   
   beginShape();
   vertex(0, 0);
   bezierVertex(0, 0, 0, myRadius, 0, myRadius);
 //  bezierVertex(2*myAngle/PI *.55*myRadius, myRadius, myRadius, 2*myAngle/PI*.55*myRadius, myRadius*sin(myAngle),myRadius*cos(myAngle));  
   bezierVertex(_smallR, myRadius,myRadius*sin(myAngle) -_smallR*cos(myAngle) ,myRadius*cos(myAngle) +_smallR*sin(myAngle) , myRadius*sin(myAngle),myRadius*cos(myAngle));  
   endShape(CLOSE);
   
   /*
   stroke(0,255,0);
   line( 0, myRadius,_smallR, myRadius);
   line(myRadius*sin(myAngle),myRadius*cos(myAngle),myRadius*sin(myAngle) -_smallR*cos(myAngle) ,myRadius*cos(myAngle) +_smallR*sin(myAngle) );
  */
 
  popMatrix();
  
   }
   
  
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "pieLife4" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
